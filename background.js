
///////////////////////////////////////////////////////////////////
///info :  ////////////////////////////////////////////////////////
// to add as a temporary add-on on firefox, go to about:debugging
///////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////


//TODO List 
//make sure all images can be reloaded (by calculating the position T_T)


let portFromScript;

var allowOneRequest = false;
var isBlocking = true;
var frictionDelay = 1000; //TODO dupplicate with replacement.js but we need to communicate it to popup.js

const iconActive = { path: "icon/iconactive2.svg" }
const iconInactive = { path: "icon/iconinactive2.svg" }

var urlPattern = "<all_urls>"; //var pattern = "*://*/*" //all urls except files on the computer



/*/Initialization//////////////////////////*/
//we activate blocking
//we listen for image requests and we block them by default
browser.webRequest.onBeforeRequest.addListener(
  block,
  {urls:[urlPattern], types:["image", "object", "imageset"]},
  ["blocking"]
);


function block(requestDetails) {

	//the actual blocking happens here
	//we check whether the image was explicitely asked for
  	if (allowOneRequest == false){
  		//if not, we cancel it
  		//console.log("blocking the following image: " + requestDetails.url);
  		return {
      		cancel: true
   		};
  	}
	else{
	  	console.log("not blocking:" + requestDetails.url);
	  	allowOneRequest = false;
	}
};

function toogleBlocking(){

	//we received the command from popup.js
	let currentTab = undefined;
	var commandContent = "";

	if(isBlocking === true){
		browser.webRequest.onBeforeRequest.removeListener(block);
		commandContent = "desactivateBlocking";
		isBlocking = false;
		browser.browserAction.setIcon(iconInactive).then();
	}
	else if(isBlocking === false){
		browser.webRequest.onBeforeRequest.addListener(
		  block,
		  {urls:[urlPattern], types:["image", "object", "imageset"]},
		  ["blocking"]
		);
		commandContent = "activateBlocking";
		isBlocking = true;
		browser.browserAction.setIcon(iconActive).then();
	}

	//we forward the information to replacement.js
	portFromScript.postMessage(
		{
		command: commandContent
		}
	);
}

function updateDelay(newValue){

	//we forward the new delay to replacement.js
	portFromScript.postMessage(
		{
		command: "updateDelay",
	    value: newValue,
		}
	);
}


//we listen to the message sent by replacement.js
browser.runtime.onConnect.addListener(connected);

function connected(p) {
  portFromScript = p;
  portFromScript.onMessage.addListener(messageCenter);
}

function messageCenter(message){

	//we receive messages from replacement.js
	//console.log("in background, I received a message");
	if (message.command === "requestImage"){
		allowOneRequest = message.value;
	}
	else if(message.command === "checkStatus"){
			portFromScript.postMessage(
				{
			        command: "initialBlockingStatus",
			        value: isBlocking,
	    		});
			updateDelay(frictionDelay);
	}
	else{
		console.log("I'm background.js and I didn't get the message from replacement.js, sorry");
	}
}


//we listen to the message sent by popup.js
browser.runtime.onMessage.addListener(popupListener);

function popupListener(message){

	//we receive messages from popup.js
	if(message.command === "toogleImageBlocking" ){
		toogleBlocking();
	}
	else if(message.command === "updateDelay"){
		updateDelay(message.value);
		frictionDelay = message.value;
	}
	else{
		console.log("I'm background.js and I didn't get the message from popup, sorry");
	}
}

function communicateBlockingStatus(){
	//we communicate blocking status to popus.js
	return isBlocking;
}

function communicateDelayValue(){
	//we communicate delay value to popup.js
	return frictionDelay;
}



