[fr] Imagéco
============
Imagéco est une petite extension qui bloque les images et vous permet de les télécharger à la demande, lorsque vous survolez les images avec le curseur.
Cette extension est actuellement en développement, elle ne fonctionne pas encore avec tous les types d'images et des bugs sont certainement toujours présents.

Comment tester Imagéco
----------------------
Imagéco ne fonctionne pour l'instant que sous firefox.

① télécharger l'extension  depuis framagit

② dans firefox, aller à l'adresse about:debugging

③ cliquer sur charger un module temporaire puis sélectionner le fichier manifeste.json dans le dossier téléchargé (n’importe quel fichier devrait fonctionner)

④ le plug-in est activé, il est possible de le tester mais il ne sera actif que pendant la session, il faut refaire la manipulation à chaque réouverture du navigateur

Participer au développement
----------------------------
Parce que chaque page traite les images différemment, Imagéco ne fonctionne pas encore dans tous les cas.
Votre contribution est la bienvenue, en particulier sur les bugs suivants :
- Imagéco ne sait pas encore demander l'affichage des background-image 
- Certains sites modifie le DOM (et enlèvent l'adresse de l'image) lorsqu'ils s'aperçoivent que les images n'ont pas été chargées
- Quand les images sont nichées dans d'autres éléments comme des boutons, ceux-ci interceptent la demande d'affichage


[en] Imagéco
============
Imagéco is a tiny image blocker extension for firefox that lets you request images on demand.
This extension is currently under development so it might not work with some images yet.

How to test Imagéco
----------------------
Imagéco currently only works with firefox.

① download the extension from framagit.

② in firefox, go to the address about:debugging

③ clic on load temporary add-on and select the manifest.json file in the downloaded folder.

④ the plug-in should now be active. You can test it but it will only be activated during a session. You will have to redo these operations everytime you reopen your browser.

Contribute
----------------------------
Because each webpage treat images differently, there are still many cases in which Imagéco doesn't work.
Any contribution will be appreciated but the following bugs are especially crucial:
- Imagéco doesn't yet know how to ask for background-images to reload
- Some website modify the DOM (and remove image addresses) when they catch loading error events
- When images are nested within other elements like buttons, the event gets intercepted