

//parameters
var isBlockerActive = undefined;
var frictionDelay = undefined;


function initialize(){
	//we need to add the eventlistener dynamically for security policy reason
	var toggleButton = document.getElementById("toogle");
	toggleButton.addEventListener("click", toogleImageBlocking);
	var delaySlider = document.getElementById("delay");
	delaySlider.addEventListener("input", updateDelay);
	//we need to check our blocking status and our delay slider status with background.js
	getStatus();
}

async function getStatus(){
	//we ask background.js what is the blocking status and the delay
    let background  = await browser.runtime.getBackgroundPage();
    isBlockerActive = background.communicateBlockingStatus();
    frictionDelay = background.communicateDelayValue();


    var button = document.getElementById("toogle");
    toogleButtonStyle(button);
    updateSliderStyle(frictionDelay);
}

function updateDelay(event){
	frictionDelay = event.target.value*1000
	//we update the label value
	document.getElementById('value').innerHTML = (event.target.value+"s");
	//we send the message to background
	browser.runtime.sendMessage(
		{
        command: "updateDelay",
        value: frictionDelay,
    	}
    );
}

function toogleImageBlocking(event){
	var button = event.target;
	//we let background know that we are toogling blocking
	browser.runtime.sendMessage(
		{
        command: "toogleImageBlocking",
    	}
    );

    if(isBlockerActive === true){
    	isBlockerActive = false;
    }
    else if(isBlockerActive === false){
    	isBlockerActive = true;
    }
    else{
    	console.log("isBlockerActive is not initated in popup.js");
    }

    //we update button style
    toogleButtonStyle(button);
}

function toogleButtonStyle(button){
	//we toogle button style
	title = document.getElementById("title");
	if (isBlockerActive === false){
		title.classList.add("inactive");		
		button.classList.add("off");
		if(button.classList.contains("on")){
			button.classList.remove("on");
			title.classList.remove("active");
		};
		button.innerText = "activer";
		title.innerText = "imagéco désactivé";
	}
	else if (isBlockerActive === true){
		if(button.classList.contains("off")){
			button.classList.remove("off");
			title.classList.remove("inactive");
		};
		button.classList.add("on");
		title.classList.add("active");
		button.innerText = "désactiver";
		title.innerText = "imagéco activé";
	}
	else{
		console.log("isBlockerActive is not initated in popup.js");
	}
}

function updateSliderStyle(){
	var delayLabel = document.getElementById('value');
	var delaySlider = document.getElementById('delay');
	delaySlider.value = frictionDelay/1000;
	delayLabel.innerText = frictionDelay/1000 + "s";

}

initialize();