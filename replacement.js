//known issues with images

//when images are nested, they sometimes don't catch the event and can't be reloaded
//instagram : no icons (<img> are just removed from the page...)
//instagram, can't display all when several images (arrows stay behind)
//for now, we can't reload background-images

//when images load because they were in the cache, we are still applying the imgPlaceholder style


//parameters
var frictionDelay = 1000;

//global variables 
var timer = undefined;
var myPort = undefined;
var imagePlaceholderStyle = `
    img::after {
	    z-index: 0;
	}
	.imgPlaceholder:hover{
		border: 1px solid seagreen !important;
	}
	.imgPlaceholder { 
		opacity: 1;
		box-sizing: border-box;
		font-size: .8em;
		color: #888;
	    line-height: 1.5;
	    font-style: italic;
    	min-width: 5px !important;
    	min-height: 5px !important;
    	z-index: 200 !important;
    	background-color: #eee;
    	border: 1px solid #eee !important;
    }
`

function initialize() {

	//we listen for future image loading errors
	document.body.addEventListener("error", catchImgLoadingError, true);
	////[WIP] checking images through position
	document.addEventListener('keydown', whereIsCursor);

	//we initialize the messaging channel with background script
	myPort = browser.runtime.connect({name:"port-from-cs"});
	myPort.onMessage.addListener(messageCenter);

	//we load the placeholder CSS
	var styleSheet = document.createElement("style");
	styleSheet.type = "text/css";
	styleSheet.innerText = imagePlaceholderStyle;
	document.head.appendChild(styleSheet);

    myPort.postMessage({command: "checkStatus"});
}

//we add the CSS and prepare images whenever the script loads
initialize();

////[WIP] checking images through position
function whereIsCursor(event){
	//console.log("target" +event.target);
	console.log("originalTarget" +event.originalTarget);
	var images = document.getElementsByTagName('img');
	var cursorX = event;
	console.log(cursorX);
	var imgRect = images[i].getBoundingClientRect();
	console.log(imgRect);
	//TODO while serait mieux
	for (i=0; i<images.length; i++){
		console.log("cursorX" + cursorX);
		console.log("imgRect.right" + imgRect.right);
		console.log("imgRect.left" + imgRect.left);
		if((cursorX < imgRect.right) && (cursorX > imgRect.left)){
			console.log("im inside");
		}
		else{
			console.log("im not in " + imgRect);
		}
	};
}

function prepareAllImages(){
	console.log("im in prepareAllImages");
	//we prepare existing images for future user requests
	//TODO we should optimize for doing this in a lazyloading way
	//TODO ideally, we should only use catchImgLoadingError but some images manage to go through (par exemple mediapart.fr)...
	var images = document.getElementsByTagName('img');
	for (i=0; i<images.length; i++){
		//CHECK: why only images[0], can't remember why I did that...
		if((images[0].naturalWidth === 0) || (!images[0].complete)){
			prepareImage(images[i]);
		}
	};
}

function prepareImage(image){
	console.log("im in prepareImage");
	//we prepare existing images for future user requests
	image.addEventListener("mouseenter", requestImageBuffer, true);
	image.addEventListener("mouseout", exitingImage, true);

	//we style image placeholders
	image.classList.add("imgPlaceholder");
	if (window.getComputedStyle(image).position == "static"){
    	image.style.setProperty('position', 'relative');
    };
    if (window.getComputedStyle(image).display == "inline"){
    	image.style.setProperty('display', 'block');
    };
}

function catchImgLoadingError(event){
	console.log("im in catchImgLoadingError");
	event.stopPropagation();
	//we catch newly loaded images
	image = event.target;
	prepareImage(image);
	//console.log("catch error " + image.src);
}

function requestImageBuffer(event){
	//console.log("im in requestImageBuffer");
	image = event.target;
	timer = window.setTimeout(function() {allowOneRequest(image);}, frictionDelay);
}

function exitingImage(event){

	//if the cursor exits the image before the end of the timer, we abort requestImage
	clearTimeout(timer);
}

function cleanImage(image){

	//we reassign the image adress to force the browser to reload it.
	image.src = image.src;
	//we clean the image
	image.classList.remove("imgPlaceholder");
	image.removeEventListener("mouseenter", requestImageBuffer);
	image.removeEventListener("mouseout", exitingImage);

	//SPECIAL FIX for medium (they otherwise make the image invisible)
	image.style.setProperty('opacity', '1');
}

function allowOneRequest(image){

	//we tell background.js not to block this specific image request
	console.log("we should not be blocking the following request");
	//console.log(image.src);
	myPort.postMessage({
		command: "requestImage",
        value: true
    });
    //we clean this image
    cleanImage(image);
}

//we listen to the message sent by background.js
function messageCenter(message){

	if(message.command === "initialBlockingStatus"){
		if(message.value === true){
			//console.log("In content script, we received a true initialBlockingStatus");
			prepareAllImages();
		}
	}
	else if(message.command === "desactivateBlocking"){
		//console.log("ive received the desactivateBlocking command in replacement.js");
		//we stop listening for image loading errors and reload all images
		document.body.removeEventListener("error", catchImgLoadingError);
		reloadAllImages();
		isBlocking = false;
	}
	else if(message.command === "activateBlocking"){
		//console.log("ive received the activateBlocking command in replacement.js");
		document.body.addEventListener("error", catchImgLoadingError, true);
		isBlocking = true;
	}
	else if(message.command === "updateDelay"){
		//console.log("ive received the updateDelay command in replacement.js");
		//we update friction delay with new value
		frictionDelay = message.value;
	} 
	else{
		console.log("I didn't understand the message sent");
	}
}

function reloadAllImages(){
	var images = document.getElementsByTagName('img');
	for (i=0; i<images.length; i++){
		cleanImage(images[i]);
	};
}
